
# coding: utf-8

# In[1]:

import project1
import sub_project2
from sklearn.svm import SVC


# In[2]:

X, y = project1.get_data()


# In[3]:

clf = SVC()


# In[4]:

clf.fit(X, y)


# In[5]:

y_pred = clf.predict(X)


# In[6]:

if sub_project2.verify_results(y, y_pred):
    print("Done OK!")
else: 
    print("Done - not OK!")


# In[ ]:



